from googleapiclient import discovery
from google.oauth2 import service_account
import datetime
import time
import pytz
import os
import json

def backup(request):
    tz = pytz.timezone('America/Sao_Paulo')
    x = datetime.datetime.now(tz)
    date = x.strftime("%Y-%m-%d %H:%M:%S")
    date = date.replace(' ', '_')

    credentials = service_account.Credentials.from_service_account_file('key.json')

    service = discovery.build('sqladmin', 'v1beta4', credentials=credentials)
    project = os.environ['PROJECT_ID']
    instance = os.environ['INSTANCE']
    bucket = os.environ['BUCKET']

    databases = json.loads(os.environ['DATABASES'])
    print("realizando o backup dos seguintes schmeas: \n"+'\n'.join(map(str, databases)))

    for i in databases:
        instances_export_request_body = {
        "exportContext": {
            "databases": [
            i
            ],
            "uri": bucket+"/"+i+"-"+date+".sql"
        }
        }
        request = service.instances().export(project=project, instance=instance, body=instances_export_request_body)
        response = request.execute()
        operation = response['name']
        time.sleep(5)
        request = service.operations().get(project=project, operation=operation)
        response = request.execute()
        a=0
        while response['status'] != 'DONE':
            time.sleep(5)
            request = service.operations().get(project=project, operation=operation)
            response = request.execute()
            print("status do backup do schema "+i+": "+response['status'])
        else:
            print("backup do schema "+i+" finalizado com sucesso")
    return("Backups realizado com sucesso dos schemas: \n"+'\n'.join(map(str, databases)))
