## Name
backup-automation for GCP

## Description
This repository creates a backup-automation on pricipal database schemas of any database on GCP.
This repository uses Gcloud Functions 

## Pipeline Status

### main
[![pipeline status](https://gitlab.com/brius/sre/devops/backup-automation/badges/main/pipeline.svg)](https://gitlab.com/brius/sre/devops/backup-automation/-/commits/main)


## Usage

The usage of this applications is automatic by scheduling of the GCP Scheduler, each schedule is database oriented:
- backup-schemas for mysql-prod-1
- backup-schemas-2 for mysql-prod-3

Each of these schedules start a respectively function (this code) that creates a backup from certainly database schemas and send to a bucket bucket of 7 day retention.

### Pre reqs
All the Bucket and instance create should have the correct permissions

### Envs to replace in GCP Functions

PROJECT_ID -> your GCP Project ID
INSTANCE -> GCP SQL Instance name
BUCKET -> GCP Bucket Name

## Roadmap

- Create more granularity for the backup of the application
- Send notification on Discord 

## Authors and acknowledgment
[@naynivek](https://gitlab.com/naynivek)


